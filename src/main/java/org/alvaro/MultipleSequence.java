package org.alvaro;

import java.util.List;

/**
 * This interface represents classes implementing multiple sequences.
 * Example of three sequences:
 *  Id1 IAGGEAITTGGSRCSLGFNVVAHALTAGHCTNISAWSIGTRTGTSFNNDY
 *  Id2 ALKLEADRLFDVKNEDGDVIGHALAMEGKVMKPLHVKGTIDHPVLSKLKF
 *  Id3 TKSSAYDMEFAQLPVNMRSEAFTYTSEHPEGFYNWHHGAVQYSGGRFTIP
 *
 * @author name of the author here <name@email.com>
 */
public interface MultipleSequence {

  /**
   * Returns the number of sequences
   * @return
   */
  public int getNumberOfSequences() ;

  /**
   * Returns the description of the ith-sequence ith
   * @param sequenceIndex
   * @return
   */
  public String getDescriptionAt(int sequenceIndex) ;

  /**
   * Returns the ith-sequence data
   * @param sequenceIndex
   * @return
   */
  public String getSequenceData(int sequenceIndex) ;

  /**
   * Reads the sequences from a Fasta file
   * @param fileName
   */
  public void readFromFastaFile(String fileName) ;

  /**
   * Writes the sequences to a Fasta file
   * @param fileName
   */
  public void writeToFastaFile(String fileName) ;

  /**
   * Reads the multiple sequence from UnitPorot
   * @param unitProtId The identifier of the sequence in UniProt
   */
  public void readFromUniProt(List<String> unitProtId) ;

  /**
   * Returns a string representing the multiple sequence
   * @return
   */
  @Override
  public String toString() ;
}
